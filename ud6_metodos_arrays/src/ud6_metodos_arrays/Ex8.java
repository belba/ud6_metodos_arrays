package ud6_metodos_arrays;

import java.util.Scanner;

public class Ex8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 8) Crea un array de 10 posiciones de n�meros con valores pedidos por teclado. Muestra por
			consola el indice y el valor al que corresponde. Haz dos m�todos, uno para rellenar valores y
			otro para mostrar.
		 */				
		
		leerValores();
	}

	private static void leerValores() {	
		
		int[] valorLeer = rellenarValores();
		
		for (int i = 0; i < valorLeer.length; i++) {
			
			System.out.println("Valores: "+valorLeer[i]);
		}
		
	}

	private static int[] rellenarValores() {
		
		int i = 0;
		int[] valores = new int[10];
		
		while(i < 10) {			
			Scanner valor = new Scanner(System.in); 
			System.out.print("Introduce valores (Max 10): ");
			
			int valoresIntro = valor.nextInt();
			
			valores[i] = valoresIntro;
			i++;
			 
		}
		return valores;
		
	}

}
