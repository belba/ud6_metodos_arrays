package ud6_metodos_arrays;

import java.util.Scanner;

public class Ex9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * 9) Crea un array de n�meros donde le indicamos por teclado el tama�o del array,
			rellenaremos el array con n�meros aleatorios entre 0 y 9. Al final muestra por pantalla el
			valor de cada posici�n y la suma de todos los valores.
			
			Tareas a realizar: Haz un m�todo para rellenar el array(que tenga como par�metros los
			n�meros entre los que tenga que generar) y otro para mostrar el contenido y la suma del
			array
		 */
		
		leerValors();
		
		}
		
		private static void leerValors() {
			
			int[] valoresAleatorios = rellenarValores();
			
			for (int i = 0; i < valoresAleatorios.length; i++) {
				System.out.println("Valores generados en array : "+valoresAleatorios[i]);
			}
		
		}

		private static int[] rellenarValores() {
			
			Scanner valor = new Scanner(System.in); 
			System.out.print("Introduce el tama�o del array: ");
			
			
			int valoresIntro = valor.nextInt();
			
			int[] valores = new int[valoresIntro];
			
			for (int i = 0; i < valores.length; i++) {
				
				 int valorGenerado = (int) Math.floor(Math.random()*valoresIntro+1);
				 valores[i] = valorGenerado;				 
				 
			}
			
			return valores;
			
		}
	}

