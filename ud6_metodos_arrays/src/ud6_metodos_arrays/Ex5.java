package ud6_metodos_arrays;

import java.util.Scanner;

public class Ex5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*5) Crea una aplicaci�n que nos convierta un n�mero en base decimal a binario. Esto lo
			realizara un m�todo al que le pasaremos el numero como par�metro, devolver� un String
			con el numero convertido a binario. Para convertir un numero decimal a binario, debemos
			dividir entre 2 el numero y el resultado de esa divisi�n se divide entre 2 de nuevo hasta que
			no se pueda dividir mas, el resto que obtengamos de cada divisi�n formara el numero
			binario, de abajo a arriba.
		 */
		
		Scanner numero = new Scanner(System.in); 
		System.out.print("Introduce un n�mero en base decimal: ");
		int numerodecimalIntro = numero.nextInt();
		
		convertirDecimalBinario(numerodecimalIntro);
		
		numero.close();
	}
	
	public static void convertirDecimalBinario(int numerodecimalIntro) {
		String binario = "";
		
			while(numerodecimalIntro > 0) {
				
				if (numerodecimalIntro % 2 == 0) {
	                binario = "0" + binario;
	            } else {
	                binario = "1" + binario;
	            }
				numerodecimalIntro = numerodecimalIntro / 2;
			}
	
		System.out.println("N�mero binario: "+binario);
	}

}
