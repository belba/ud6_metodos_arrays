package ud6_metodos_arrays;

import java.util.Scanner;

public class Ex12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * 12) Crea un array de n�meros de un tama�o pasado por teclado, el array contendr�
			n�meros aleatorios entre 1 y 300 y mostrar� aquellos n�meros que acaben en un d�gito que
			nosotros le indiquemos por teclado (debes controlar que se introduce un numero correcto),
			estos deben guardarse en un nuevo array.
			
			Por ejemplo, en un array de 10 posiciones le indicamos mostrar los n�meros acabados en 5,
			podr�a salir 155, 25, etc.
		 */
		
		Scanner valor = new Scanner(System.in); 
		System.out.print("Introduce el tama�o del array: ");
		
		int tanyoArray = valor.nextInt();
		
		
		MostrarNumerosAcabadosDigitos(tanyoArray);
		
	}
	
	private static void MostrarNumerosAcabadosDigitos(int tanyoArray) {
		
		Scanner valor = new Scanner(System.in); 
		System.out.print("Introduce el ultimo digito: ");
		
		int ultimoDigitoIntro = valor.nextInt();
		
		int arrayValoresAleatorios[] = rellenarArray(tanyoArray);
		int ultimoValor = 0;
		
		for (int i = 0; i < arrayValoresAleatorios.length; i++) {			
			ultimoValor = arrayValoresAleatorios[i]%10;
			
			if(ultimoDigitoIntro == ultimoValor)
				System.out.println("Numero acabado en "+ ultimoDigitoIntro +": "+arrayValoresAleatorios[i]);
			
		}
		
	}

	private static int[] rellenarArray(int tanyoArray) {
		
		int[] array = new int[tanyoArray];

		for (int i = 0; i < array.length; i++) {
			int valoresGenerados = (int) Math.floor(Math.random()*300+1);
			
			array[i] = valoresGenerados;
		}
		
		return array;
	}

}
