package ud6_metodos_arrays;

import java.util.Scanner;

public class Ex6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* 6) Crea una aplicaci�n que nos cuente el n�mero de cifras de un n�mero entero positivo
			(hay que controlarlo) pedido por teclado. Crea un m�todo que realice esta acci�n, pasando
			el n�mero por par�metro, devolver� el n�mero de cifras
		*/
		
		Scanner numero = new Scanner(System.in); 
		System.out.print("Introduce un n�mero a cifrar: ");
		int numeroIntro = numero.nextInt();
		
		cifrasDeNumeros(numeroIntro);
		numero.close();
	}
	
	public static void cifrasDeNumeros(int numeroIntro) {
		if(numeroIntro >= 0) {
			String numeroCifrado = Integer.toString(numeroIntro);
			System.out.println("Cifras del un n�mero "+ numeroIntro +" es: "+numeroCifrado.length());
		}else {
			System.out.println("El n�mero es negativo");
		}
		
		
	}
	

}
