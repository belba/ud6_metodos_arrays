package ud6_metodos_arrays;

import java.util.Scanner;

public class Ex3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/* 3) Crea una aplicaci�n que nos pida un n�mero por teclado y con un m�todo se lo pasamos
			por par�metro para que nos indique si es o no un n�mero primo, debe devolver true si es
			primo sino false.
			Un n�mero primo es aquel solo puede dividirse entre 1 y si mismo. Por ejemplo: 25 no es
			primo, ya que 25 es divisible entre 5, sin embargo, 17 si es primo.
		 */
		
		Scanner numero = new Scanner(System.in); 
		System.out.print("Introduce un n�mero: ");
		int numeroIntro = numero.nextInt();
		
		numeroPrimo(numeroIntro);
		numero.close();
	}
	
	public static void numeroPrimo(int numeroIntro) {
		boolean numerosPrimos = numeroIntro%2 == 0;
		System.out.println(numerosPrimos);
		
		if(numerosPrimos) {
			System.out.println("El n�mero "+numeroIntro+" es primo: ");
		}else {
			System.out.println("El n�mero "+numerosPrimos+" no es primo: ");
		}
		
	}

}
