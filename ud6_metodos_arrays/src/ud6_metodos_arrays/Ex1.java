package ud6_metodos_arrays;

import java.util.Scanner;

public class Ex1 {

	public static void main(String[] args) {
		/*1) Crea una aplicaci�n que nos calcule el �rea de un circulo, cuadrado o triangulo. Pediremos
			que figura queremos calcular su �rea y seg�n lo introducido pedir� los valores necesarios
			para calcular el �rea. Crea un m�todo por cada figura para calcular cada �rea, este devolver�
			un n�mero real. Muestra el resultado por pantalla.
			Aqu� te mostramos que necesita cada figura:
			
			Circulo: (radio^2)*PI
			Triangulo: (base * altura) / 2
			Cuadrado: lado * lado */
		
		calcularFiguras();

	}
	
	private static void calcularFiguras() {
		Scanner figura = new Scanner(System.in); 
		System.out.print("1) Calcular Figura Circulo"
				+ "\n2) Calcular Figura Triangulo"
				+ "\n3) Calcular Figura Cuadrado"
				+ "\nIntroduce una opci�n: ");
		int figuraSelect = figura.nextInt();
		
		switch (figuraSelect) {
		case 1:
			calcularCirculo();
			break;
			
		case 2:
			calcularAngulo();
			break;
			
		case 3:
			calcularTriangulo();
			break;
			
		default:
			System.out.print("Opci�n incorrecta");	
			break;
		}
		
		figura.close();
		
	}

	public static void calcularCirculo() {
		Scanner circulo = new Scanner(System.in);
		System.out.print("Introduce el radio: ");
		
		Double circuloIntro = circulo.nextDouble();
		
		System.out.println("Resultado de un circulo: "+ Math.pow(circuloIntro, 2)*Math.PI);
		
		circulo.close();
	}
	
	public static void calcularAngulo() {
		Scanner trianguloBase = new Scanner(System.in);
		System.out.print("Introduce la Base: ");			
		Double trianguloBaseIntro = trianguloBase.nextDouble();

		Scanner trianguloAltura = new Scanner(System.in);
		System.out.print("Introduce la Altura: ");			
		Double trianguloAlturaIntro = trianguloAltura.nextDouble();
		
		System.out.println("Resultado de un Triangulo: "+ (trianguloBaseIntro * trianguloAlturaIntro) / 2);
		trianguloBase.close();
	}

	public static void calcularTriangulo() {
		Scanner cuadrado = new Scanner(System.in);
		System.out.print("Introduce la base o altura: ");			
		Double cuadradoIntro = cuadrado.nextDouble();
		
		System.out.println("Resultado de un Triangulo: "+ cuadradoIntro * cuadradoIntro);
		cuadrado.close();
	}
}
