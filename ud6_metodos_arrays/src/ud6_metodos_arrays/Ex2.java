package ud6_metodos_arrays;

import java.util.Scanner;

public class Ex2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*2) Crea una aplicaci�n que nos genere una cantidad de n�meros enteros aleatorios que
			nosotros le pasaremos por teclado. Crea un m�todo donde pasamos como par�metros entre
			que n�meros queremos que los genere, podemos pedirlas por teclado antes de generar los
			n�meros. Este m�todo devolver� un n�mero entero aleatorio. Muestra estos n�meros por
			pantalla*/
		
		Scanner numero = new Scanner(System.in); 
		System.out.print("Introduce un valor para generar aleatorio: ");
		int numeroAleatorio = numero.nextInt();
		
		generarNumeroAleatorio(numeroAleatorio);
		numero.close();
	}
	
	public static void generarNumeroAleatorio(int numeroAle) {
		
		int resultadoGenerado = (int)(Math.random() * numeroAle) + 1; //Generador de numero aleatorio entre 1 al n�mero introducido por pantalla
		System.out.println("N�mero generado aleatorio entre 1 y "+numeroAle+" es: "+ resultadoGenerado);
		
	}

}
