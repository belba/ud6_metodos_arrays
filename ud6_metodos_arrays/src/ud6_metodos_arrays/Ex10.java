package ud6_metodos_arrays;

import java.util.Scanner;

public class Ex10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* 10) Crea un array de n�meros de un tama�o pasado por teclado, el array contendr�
			n�meros aleatorios primos entre los n�meros deseados, por �ltimo nos indicar cual es el
			mayor de todos.
			Haz un m�todo para comprobar que el n�mero aleatorio es primo, puedes hacer todos lo
			m�todos que necesites.

		 */
		Scanner valor = new Scanner(System.in); 
		System.out.print("Introduce el tama�o del array: ");
		
		int tanyoArray = valor.nextInt();
		
		int[] array = new int[tanyoArray];
		
		
		
		for (int i = 0; i < tanyoArray; i++) {	
			
			int valoresGenerados = (int) Math.floor(Math.random()*tanyoArray+1);
			
			if(comprobarNumeroPrimero(valoresGenerados) == true) {
				array[i] = valoresGenerados;
			}
		}
		
		int numeroMayor = 0;
		
		for (int i = 0; i < array.length; i++) {
			if(array[i]>numeroMayor){ // 
				numeroMayor = array[i];
		    }
		}
		
		System.out.print("El n�mero mayor de la array de primos es: "+numeroMayor);
	}

	private static boolean comprobarNumeroPrimero(int valoresGenerados) {		
		if(valoresGenerados%2==0) {
			return false;
		}else {
			return true;
		}
	}

}
