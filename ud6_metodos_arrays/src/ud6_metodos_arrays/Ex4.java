package ud6_metodos_arrays;

import java.util.Scanner;

public class Ex4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*4) Crea una aplicaci�n que nos calcule el factorial de un n�mero pedido por teclado, lo
			realizara mediante un m�todo al que le pasamos el n�mero como par�metro. Para calcular
			el factorial, se multiplica los n�meros anteriores hasta llegar a uno. Por ejemplo, si
			introducimos un 5, realizara esta operaci�n 5*4*3*2*1=120.  */
		
		Scanner numero = new Scanner(System.in); 
		System.out.print("Introduce un n�mero: ");
		int numeroIntro = numero.nextInt();
		
		calcularFactorial(numeroIntro);
		numero.close();
	}
	
	public static void calcularFactorial(int numeroIntro) {
		int resultadoFact = 1;
		
		for (int i = 1; i <= numeroIntro; i++) {
			resultadoFact = resultadoFact*i;
			
		}
		
		System.out.println("Resultado Factorail: "+resultadoFact);
	}

}
