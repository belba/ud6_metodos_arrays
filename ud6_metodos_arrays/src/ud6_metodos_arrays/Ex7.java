package ud6_metodos_arrays;

import java.util.Scanner;

public class Ex7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* 7) Crea un aplicaci�n que nos convierta una cantidad de euros introducida por teclado a otra
			moneda, estas pueden ser a dolares, yenes o libras. El m�todo tendr� como par�metros, la
			cantidad de euros y la moneda a pasar que sera una cadena, este no devolver� ning�n valor,
			mostrara un mensaje indicando el cambio (void).
		 */
		
		Scanner cantidad = new Scanner(System.in); 
		System.out.print("Introduce un n�mero en base decimal: ");
		int cantidadEuroIntro = cantidad.nextInt();
		
		Double libras = 0.86;
		Double dolar = 1.28611;
		Double yenes = 129.852;
		
		convertidorMoneda(cantidadEuroIntro, libras, dolar, yenes);
		cantidad.close();
		
	}
	
	public static void convertidorMoneda(int cantidadEuroIntro, Double libras, Double dolar, Double yenes) {
		
		System.out.println("El cambio de euro "+cantidadEuroIntro+" a libras: "+(cantidadEuroIntro*libras));		
		System.out.println("El cambio de euro "+cantidadEuroIntro+" a dolar: "+(cantidadEuroIntro*dolar));		
		System.out.println("El cambio de euro "+cantidadEuroIntro+" a yenes: "+(cantidadEuroIntro*yenes));		
		
		
	}

}
